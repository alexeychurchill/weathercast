package io.github.alexeychurchill.weathercards.view.weather;

public interface WeatherView {
    long provideCityId();
    void showWeather(WeatherViewModel weatherViewModel);
    void showWaiting();
}
