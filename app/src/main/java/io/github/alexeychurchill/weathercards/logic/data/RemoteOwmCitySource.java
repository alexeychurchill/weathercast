package io.github.alexeychurchill.weathercards.logic.data;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import io.github.alexeychurchill.weathercards.logic.HttpErrorRuntimeException;
import io.github.alexeychurchill.weathercards.logic.OpenWeatherMapCityApi;
import io.github.alexeychurchill.weathercards.logic.pojo.City;
import io.github.alexeychurchill.weathercards.logic.specific_pojo.OwmCity;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RemoteOwmCitySource implements OwmCitySource {
    private static final String BASE_URL = "http://openweathermap.org";

    private static final String NEW_LINE = "\n";
    private static final String TABULATION = "\t";

    private static final int INDEX_OWM_ID = 0;
    private static final int INDEX_NAME = 1;
    private static final int INDEX_LATITUDE = 2;
    private static final int INDEX_LONGITUDE = 3;
    private static final int INDEX_COUNTRY_CODE = 4;

    private OpenWeatherMapCityApi mCityApi;

    public RemoteOwmCitySource() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        mCityApi = retrofit.create(OpenWeatherMapCityApi.class);
    }

    @Override
    public List<OwmCity> getCities() throws IOException {
        Call<String> cityListCall = mCityApi.getCityList();
        Response<String> cityListResponse = cityListCall.execute();
        if (!cityListResponse.isSuccessful()) {
            throw new HttpErrorRuntimeException(
                    cityListResponse.code(), cityListResponse.message()
            );
        }
        return cityListFromString(cityListResponse.body());
    }

    private List<OwmCity> cityListFromString(String source) {
        List<OwmCity> owmCities = new LinkedList<>();
        String[] strings = source.split(NEW_LINE);
        for (int stringIndex = 1; stringIndex < strings.length; stringIndex++) {
            owmCities.add(parseCity(strings[stringIndex]));
        }
        return owmCities;
    }

    private OwmCity parseCity(String string) {
        String[] strings = string.split(TABULATION);
        long owmId = Long.parseLong(strings[INDEX_OWM_ID]);
        String name = strings[INDEX_NAME];
        double latitude = Double.parseDouble(strings[INDEX_LATITUDE]);
        double longitude = Double.parseDouble(strings[INDEX_LONGITUDE]);
        String countryCode = strings[INDEX_COUNTRY_CODE];
        return new OwmCity(owmId, new City(-1, latitude, longitude, countryCode, name));
    }
}
