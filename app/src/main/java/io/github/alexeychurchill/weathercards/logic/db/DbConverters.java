package io.github.alexeychurchill.weathercards.logic.db;

import android.content.ContentValues;
import android.database.Cursor;

import io.github.alexeychurchill.weathercards.logic.pojo.City;
import io.github.alexeychurchill.weathercards.logic.specific_pojo.OwmCity;

public final class DbConverters {
    public static ContentValues owmCityToContentValues(OwmCity owmCity) {
        ContentValues values = new ContentValues();
        values.put(DbContract.CityColumns.OWM_ID, owmCity.getOwmId());
        City city = owmCity.getCity();
        if (city == null) {
            return values;
        }
        values.put(DbContract.CityColumns._ID, owmCity.getOwmId());
        values.put(DbContract.CityColumns.CITY, city.getCity());
        values.put(DbContract.CityColumns.COUNTRY, city.getCountryCode());
        values.put(DbContract.CityColumns.LATITUDE, city.getLatitude());
        values.put(DbContract.CityColumns.LONGITUDE, city.getLongitude());
        return values;
    }

    public static OwmCity cursorToOwmCity(Cursor cursor) {
        int columnIndex;

        columnIndex = cursor.getColumnIndex(DbContract.CityColumns.OWM_ID);
        long owmId = columnIndex != -1 ? cursor.getLong(columnIndex) : -1;

        columnIndex = cursor.getColumnIndex(DbContract.CityColumns._ID);
        long id = columnIndex != -1 ? cursor.getLong(columnIndex) : -1;

        columnIndex = cursor.getColumnIndex(DbContract.CityColumns.CITY);
        String city = columnIndex != -1 ? cursor.getString(columnIndex) : null;

        columnIndex = cursor.getColumnIndex(DbContract.CityColumns.COUNTRY);
        String country = columnIndex != -1 ? cursor.getString(columnIndex) : null;

        columnIndex = cursor.getColumnIndex(DbContract.CityColumns.LATITUDE);
        double latitude = columnIndex != -1 ? cursor.getDouble(columnIndex) : 0.0;

        columnIndex = cursor.getColumnIndex(DbContract.CityColumns.LONGITUDE);
        double longitude = columnIndex != -1 ? cursor.getDouble(columnIndex) : 0.0;

        return new OwmCity(owmId, new City(id, latitude, longitude, country, city));
    }
}
