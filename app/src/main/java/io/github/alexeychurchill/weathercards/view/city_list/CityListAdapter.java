package io.github.alexeychurchill.weathercards.view.city_list;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaDataSource;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.alexeychurchill.weathercards.R;

class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.ViewHolder> {
    public interface OnItemClickListener {
        void onItemClicked(int position);
    }

    private List<CityViewModel> mData;
    private OnItemClickListener mItemClickListener;
    private Object mSearchHighlightSpan;

    public CityListAdapter(Context context) {
        mSearchHighlightSpan = new SearchHighlightSpan(
                context,
                R.dimen.text_highlight_search_round,
                R.color.colorAccent, R.color.colorWhite
        );
    }

    public void setItemClickListener(OnItemClickListener listener) {
        this.mItemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.layout_item_city, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mData.get(position), position, mItemClickListener, mSearchHighlightSpan);
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public void setData(List<CityViewModel> data) {
        if (mData == null && data != null) {
            mData = data;
            notifyItemRangeInserted(0, mData.size());
            return;
        }

        if (data == null && mData != null) {
            notifyItemRangeRemoved(0, mData.size());
            mData = null;
            return;
        }

        if (data == null) {
            return;
        }

        int oldLength = mData.size();
        mData = data;
        int newLength = mData.size();

        notifyNewDataSet(oldLength, newLength);
    }

    private void notifyNewDataSet(int oldLength, int newLength) {
        if (oldLength > newLength) {
            int countToNotifyRemove = oldLength - newLength;
            notifyItemRangeRemoved(newLength, countToNotifyRemove);
            notifyItemRangeChanged(0, newLength);
            return;
        }
        if (oldLength < newLength) {
            int countToNotifyAdd = newLength - oldLength;
            notifyItemRangeChanged(0, oldLength);
            notifyItemRangeInserted(oldLength, countToNotifyAdd);
            return;
        }
        notifyItemRangeChanged(0, newLength);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvCity) TextView mTvCity;
        @BindView(R.id.tvCountry) TextView mTvCountry;

        private int mPosition;
        private OnItemClickListener mClickListener;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> {
                if (mClickListener != null) {
                    mClickListener.onItemClicked(mPosition);
                }
            });
            ButterKnife.bind(this, itemView);
        }

        void bind(CityViewModel cityViewModel,
                  int position,
                  OnItemClickListener listener,
                  Object highlightSpan) {

            mPosition = position;
            mClickListener = listener;

            Spannable spannableCity = new SpannableString(cityViewModel.getCity());

            int start = cityViewModel.getCityNameSelectionStart();
            int end = cityViewModel.getCityNameSelectionEnd();
            if (start != -1 && end != -1) {
                spannableCity.setSpan(
                        highlightSpan,
                        start, end,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                );
            }

            mTvCity.setText(spannableCity, TextView.BufferType.SPANNABLE);
            mTvCountry.setText(cityViewModel.getCountryCode());
        }
    }
}
