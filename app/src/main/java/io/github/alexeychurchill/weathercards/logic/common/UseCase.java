package io.github.alexeychurchill.weathercards.logic.common;

public interface UseCase <Arg, Result> {
    interface Callback <Result> {
        void onSuccess(Result result);
        void onError(Throwable throwable);
    }

    void execute(Arg argument, Callback<Result> callback);
    void terminate();
}
