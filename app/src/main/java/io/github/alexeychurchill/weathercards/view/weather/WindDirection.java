package io.github.alexeychurchill.weathercards.view.weather;

public enum WindDirection {
    NORTH,
    NORTH_WEST,
    NORTH_EAST,
    SOUTH,
    SOUTH_WEST,
    SOUTH_EAST,
    WEST,
    EAST;

    public static WindDirection fromDegrees(double degrees) {
        if ((degrees <= 22.5 && degrees >= 0) || (degrees <= 360 && degrees > 337.5)) {
            return EAST;
        }

        if (degrees <= 67.5 && degrees > 22.5) {
            return NORTH_EAST;
        }

        if (degrees <= 112.5 && degrees > 67.5) {
            return NORTH;
        }

        if (degrees <= 157.5 && degrees > 112.5) {
            return NORTH_WEST;
        }

        if (degrees <= 202.5 && degrees > 157.5) {
            return WEST;
        }

        if (degrees <= 247.5 && degrees > 202.5) {
            return SOUTH_WEST;
        }

        if (degrees <= 292.5 && degrees > 247.5) {
            return SOUTH;
        }

        if (degrees <= 337.5 && degrees > 292.5) {
            return SOUTH_EAST;
        }

        return null;
    }
}
