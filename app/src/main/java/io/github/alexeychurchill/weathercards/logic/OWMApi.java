package io.github.alexeychurchill.weathercards.logic;

import io.github.alexeychurchill.weathercards.logic.pojo.CityForecast;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OWMApi {
    String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    @GET("forecast")
    Observable<CityForecast> getForecast(
            @Query("appid") String appId,
            @Query("id") long id
    );
}
