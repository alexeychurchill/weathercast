package io.github.alexeychurchill.weathercards.logic;

import java.util.List;

import io.github.alexeychurchill.weathercards.logic.common.UseCase;
import io.github.alexeychurchill.weathercards.logic.data.OwmCitySource;
import io.github.alexeychurchill.weathercards.logic.rx.RxCitiesUtils;
import io.github.alexeychurchill.weathercards.logic.specific_pojo.OwmCity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class CitySearchUseCase implements UseCase<String, CitySearchUseCase.SearchResult> {
    public static class SearchResult {
        private final String mQuery;
        private final List<OwmCity> mOwmCityList;

        public SearchResult(String query, List<OwmCity> owmCityList) {
            this.mQuery = query;
            this.mOwmCityList = owmCityList;
        }

        public String getQuery() {
            return mQuery;
        }

        public List<OwmCity> getOwmCityList() {
            return mOwmCityList;
        }
    }

    private OwmCitySource mCityDataSource;
    private List<OwmCity> mCityList;
    private String mCurrentQuery;
    private Disposable mRequestDisposable;

    public CitySearchUseCase(OwmCitySource dataSource) {
        this.mCityDataSource = dataSource;
    }

    @Override
    public void execute(String argument, Callback<SearchResult> callback) {
        mCurrentQuery = argument;
        if (mCityList == null) {
            requestList(callback);
            return;
        }
        callback.onSuccess(filter(mCityList, argument));
    }

    @Override
    public void terminate() {
        if (mRequestDisposable != null) {
            mRequestDisposable.dispose();
            mRequestDisposable = null;
        }
    }

    private void requestList(Callback<SearchResult> callback) {
        if (mRequestDisposable != null) {
            return;
        }
        mRequestDisposable = RxCitiesUtils.createLoadCitiesSingle(mCityDataSource)
                .subscribeOn(Schedulers.io())
                .doOnError(callback::onError)
                .doOnSuccess(owmCities -> mCityList = owmCities)
                .map(owmCities -> filter(owmCities, mCurrentQuery))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::onSuccess);
    }

    private static SearchResult filter(List<OwmCity> owmCities, String query) {
        List<OwmCity> cityList = StreamSupport.stream(owmCities)
                .filter(city -> city.getCity().getCity().contains(query))
                .collect(Collectors.toList());

        return new SearchResult(query, cityList);
    }
}
