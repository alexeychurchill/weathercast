package io.github.alexeychurchill.weathercards.view.city_list;

import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.alexeychurchill.weathercards.R;
import io.github.alexeychurchill.weathercards.logic.data.OwmCityRepository;
import io.github.alexeychurchill.weathercards.logic.data.local_city_storage.DbOwmCityRepository;
import io.github.alexeychurchill.weathercards.view.weather.WeatherActivity;

public class CityListActivity extends AppCompatActivity implements CityListView {

    private CityListPresenter mPresenter;

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.rvList) RecyclerView mRvList;
    @BindView(R.id.pbWait) ProgressBar mPbWait;

    private CityListAdapter mAdapter;

    private SearchView.OnQueryTextListener mSearchQueryTextListener =
            new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    mPresenter.performSearch(newText);
                    return false;
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_list);
        ButterKnife.bind(this);

        mPresenter = new CityListPresenterImpl();

        setSupportActionBar(mToolbar);

        mAdapter = new CityListAdapter(this);
        mAdapter.setItemClickListener(mPresenter::choose);
        mRvList.setAdapter(mAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRvList.setLayoutManager(layoutManager);
        mRvList.addItemDecoration(new DividerItemDecoration(
                this, layoutManager.getOrientation()
        ));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.detachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.city_list, menu);
        MenuItem searchMenuItem = menu.findItem(R.id.menuCityListAdd);
        SearchView searchView = ((SearchView) searchMenuItem.getActionView());
        searchView.setOnQueryTextListener(mSearchQueryTextListener);
        MenuItemCompat.setOnActionExpandListener(
                searchMenuItem,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        mPresenter.cancelSearch();
                        return true;
                    }
                }
        );
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void showLoadProgress() {
        mPbWait.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadProgress() {
        mPbWait.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showList(List<CityViewModel> items) {
        mAdapter.setData(items);
    }

    @Override
    public void showSearchView(String query) {
    }

    @Override
    public void hideSearchView() {
    }

    @Override
    public OwmCityRepository provideCityRepository() {
        return new DbOwmCityRepository(this);
    }

    @Override
    public void showForecast(long owmId) {
        WeatherActivity.start(this, owmId);
    }
}
