package io.github.alexeychurchill.weathercards.logic.specific_pojo;

import android.support.annotation.NonNull;

import io.github.alexeychurchill.weathercards.logic.pojo.City;

public class OwmCity implements Comparable<OwmCity> {
    private long owmId;
    private City city;

    public OwmCity() {
    }

    public OwmCity(long owmId, City city) {
        this.owmId = owmId;
        this.city = city;
    }

    public long getOwmId() {
        return owmId;
    }

    public void setOwmId(long owmId) {
        this.owmId = owmId;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public int compareTo(@NonNull OwmCity city) {
        return this.city.getCity().compareTo(city.getCity().getCity());
    }

    @Override
    public String toString() {
        return "OwmCity{" +
                "owmId=" + owmId +
                "city=" + city +
                "}";
    }
}
