package io.github.alexeychurchill.weathercards.logic.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import io.github.alexeychurchill.weathercards.logic.PhenomenaConverterUtil;
import io.github.alexeychurchill.weathercards.logic.pojo.Weather;

public class WeatherDeserializer implements JsonDeserializer<Weather> {
    @Override
    public Weather deserialize(
            JsonElement json, Type typeOfT, JsonDeserializationContext context
    ) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        Weather.Builder builder = new Weather.Builder();

        // Time
        builder.setTime(object.get("dt").getAsLong() * 1000); //s -> ms

        // Main sub-object
        JsonObject mainObject = object.getAsJsonObject("main");
        builder.setTemperature(mainObject.get("temp").getAsDouble())
                .setMinimalTemperature(mainObject.get("temp_min").getAsDouble())
                .setMaximalTemperature(mainObject.get("temp_max").getAsDouble())
                .setPressure(mainObject.get("pressure").getAsDouble())
                .setHumidity(mainObject.get("humidity").getAsInt());

        // Weather sub-object (array)
        JsonObject weatherPhenomenaObject = object.getAsJsonArray("weather")
                .get(0)
                .getAsJsonObject();

        int weatherPhenomenaId = weatherPhenomenaObject.get("id").getAsInt();
        builder.setPhenomena(PhenomenaConverterUtil.toPhenomena(weatherPhenomenaId));

        // Clouds sub-object
        JsonObject cloudsObject = object.getAsJsonObject("clouds");
        builder.setClouds(cloudsObject.get("all").getAsInt());

        // Wind sub-object
        JsonObject windObject = object.getAsJsonObject("wind");
        builder.setWindSpeed(windObject.get("speed").getAsDouble())
                .setWindDirection(windObject.get("deg").getAsDouble());

        return builder.build();
    }
}
