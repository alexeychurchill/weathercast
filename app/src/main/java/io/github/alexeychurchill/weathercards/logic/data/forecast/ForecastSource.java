package io.github.alexeychurchill.weathercards.logic.data.forecast;

import io.github.alexeychurchill.weathercards.logic.pojo.CityForecast;
import io.reactivex.Observable;

public interface ForecastSource {
    Observable<CityForecast> getCityForecast(long id);
}
