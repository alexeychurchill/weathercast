package io.github.alexeychurchill.weathercards.logic;

import retrofit2.Call;
import retrofit2.http.GET;

public interface OpenWeatherMapCityApi {
    @GET("help/city_list.txt")
    Call<String> getCityList();
}
