package io.github.alexeychurchill.weathercards.view.weather;

public enum PhenomenaView {
    CLOUDS,
    CLOUDY,
    DRIZZLE,
    MIST,
    RAIN,
    SHOWER_RAIN,
    SNOW,
    SUNNY,
    THUNDER
}
