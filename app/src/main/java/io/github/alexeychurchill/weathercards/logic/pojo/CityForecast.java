package io.github.alexeychurchill.weathercards.logic.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityForecast {
    @SerializedName("city")
    private City city;
    @SerializedName("list")
    private List<Weather> weatherList;

    public CityForecast() {
    }

    public CityForecast(City city, List<Weather> weatherList) {
        this.city = city;
        this.weatherList = weatherList;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<Weather> getWeatherList() {
        return weatherList;
    }

    public void setWeatherList(List<Weather> weatherList) {
        this.weatherList = weatherList;
    }
}
