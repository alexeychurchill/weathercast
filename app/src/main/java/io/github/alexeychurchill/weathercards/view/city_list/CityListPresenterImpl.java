package io.github.alexeychurchill.weathercards.view.city_list;

import java.util.List;

import io.github.alexeychurchill.weathercards.logic.CitySearchUseCase;
import io.github.alexeychurchill.weathercards.logic.LoadCityListUseCase;
import io.github.alexeychurchill.weathercards.logic.SaveCityUseCase;
import io.github.alexeychurchill.weathercards.logic.common.UseCase;
import io.github.alexeychurchill.weathercards.logic.data.OwmCityRepository;
import io.github.alexeychurchill.weathercards.logic.data.OwmCitySource;
import io.github.alexeychurchill.weathercards.logic.data.RemoteOwmCitySource;
import io.github.alexeychurchill.weathercards.logic.data.local_city_storage.MemoryOwmCityRepository;
import io.github.alexeychurchill.weathercards.logic.pojo.City;
import io.github.alexeychurchill.weathercards.logic.specific_pojo.OwmCity;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class CityListPresenterImpl implements CityListPresenter {
    private LoadCityListUseCase mLoadUsersCitiesCase;
    private CitySearchUseCase mCitySearchUseCase;
    private SaveCityUseCase mSaveCityUseCase;

    private CityListView mView;

    private String mSearchQuery;
    private boolean mSearchMode = false;
    private List<OwmCity> mOwmCities;

    private UseCase.Callback<CitySearchUseCase.SearchResult> mSearchResultCallback =
            new UseCase.Callback<CitySearchUseCase.SearchResult>() {
                @Override
                public void onSuccess(CitySearchUseCase.SearchResult result) {
                    if (mView != null) {
                        mView.hideLoadProgress();
                    }
                    mOwmCities = result.getOwmCityList();
                    showCities();
                }

                @Override
                public void onError(Throwable throwable) {
                    if (mView != null) {
                        mView.hideLoadProgress();
                    }
                }
            };

    public CityListPresenterImpl() {
        OwmCitySource owmCitySource = new RemoteOwmCitySource();
        mCitySearchUseCase = new CitySearchUseCase(new MemoryOwmCityRepository(owmCitySource));
    }

    @Override
    public void attachView(CityListView view) {
        mView = view;
        if (view == null) {
            return;
        }
        OwmCityRepository owmCityRepository = view.provideCityRepository();
        mLoadUsersCitiesCase = new LoadCityListUseCase(owmCityRepository);
        mSaveCityUseCase = new SaveCityUseCase(owmCityRepository);
        loadUsersCities();
    }

    @Override
    public void detachView() {
        if (mLoadUsersCitiesCase != null) {
            mLoadUsersCitiesCase.terminate();
            mLoadUsersCitiesCase = null;
        }

        if (mCitySearchUseCase != null) {
            mCitySearchUseCase.terminate();
            mLoadUsersCitiesCase = null;
        }

        if (mSaveCityUseCase != null) {
            mSaveCityUseCase.terminate();
            mSaveCityUseCase = null;
        }
    }

    @Override
    public void choose(int itemIndex) {
        if (mSearchMode) {
            mSearchMode = false;
            if (mView != null) {
                mView.showLoadProgress();
            }
            OwmCity owmCity = mOwmCities.get(itemIndex);
            mOwmCities = null;

            if (mView != null) {
                mView.showLoadProgress();
            }

            mSaveCityUseCase.execute(owmCity, new UseCase.Callback<OwmCity>() {
                @Override
                public void onSuccess(OwmCity owmCity) {
                    loadUsersCities();
                }

                @Override
                public void onError(Throwable throwable) {
                    if (mView != null) {
                        mView.hideLoadProgress();
                    }
                }
            });

            return;
        }

        mView.showForecast(mOwmCities.get(itemIndex).getOwmId());
    }

    @Override
    public void performSearch(String query) {
        if (mView != null) {
            mView.showLoadProgress();
        }

        mSearchMode = true;
        mSearchQuery = query;
        mCitySearchUseCase.execute(query, mSearchResultCallback);
    }

    @Override
    public void cancelSearch() {
        mSearchMode = false;
        mOwmCities = null;
        mCitySearchUseCase.terminate();
        loadUsersCities();
    }

    private void showCities() {
        if (mSearchMode) {
            mView.showList(toSearchCityList(mOwmCities, mSearchQuery));
            return;
        }
        mView.showList(toCityViewModelList(mOwmCities));
    }

    private void loadUsersCities() {
        if (mView != null) {
            mView.showLoadProgress();
            mView.showList(null);
        }
        mLoadUsersCitiesCase.execute(null, new UseCase.Callback<List<OwmCity>>() {
            @Override
            public void onSuccess(List<OwmCity> owmCities) {
                if (mView != null) {
                    mView.hideLoadProgress();
                }
                mSearchMode = false;
                mOwmCities = owmCities;
                showCities();
            }

            @Override
            public void onError(Throwable throwable) {
                if (mView != null) {
                    mView.hideLoadProgress();
                }
            }
        });
    }

    private List<CityViewModel> toCityViewModelList(List<OwmCity> owmCities) {
        return StreamSupport.stream(owmCities)
                .map(this::buildCityViewModel)
                .collect(Collectors.toList());
    }

    private CityViewModel buildCityViewModel(OwmCity owmCity) {
        return new CityViewModel(owmCity.getCity());
    }

    private List<CityViewModel> toSearchCityList(List<OwmCity> owmCities, String searchQuery) {
        return StreamSupport.stream(owmCities)
                .map(city -> buildCitySearchViewModel(city, searchQuery))
                .collect(Collectors.toList());
    }

    private CityViewModel buildCitySearchViewModel(OwmCity owmCity, String searchQuery) {
        City city = owmCity.getCity();

        CityViewModel cityViewModel = new CityViewModel(city);

        if (searchQuery != null && !searchQuery.isEmpty()) {
            String cityName = city.getCity();
            int start = cityName.indexOf(searchQuery);
            int end = start != -1 ? start + searchQuery.length() : -1;
            cityViewModel.citySelection(start, end);
        }

        return cityViewModel;
    }
}
