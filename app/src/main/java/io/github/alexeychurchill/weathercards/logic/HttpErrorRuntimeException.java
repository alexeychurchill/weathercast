package io.github.alexeychurchill.weathercards.logic;

public class HttpErrorRuntimeException extends RuntimeException {
    private int mErrorCode;
    private String mErrorDescription;

    public HttpErrorRuntimeException(int errorCode, String errorDescription) {
        this.mErrorCode = errorCode;
        this.mErrorDescription = errorDescription;
    }

    @Override
    public String getMessage() {
        return mErrorDescription + " (" + mErrorCode + ")";
    }

    public int getErrorCode() {
        return mErrorCode;
    }

    public String getErrorDescription() {
        return mErrorDescription;
    }
}
