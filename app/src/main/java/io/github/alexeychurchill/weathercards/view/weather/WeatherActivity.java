package io.github.alexeychurchill.weathercards.view.weather;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.alexeychurchill.weathercards.R;

public class WeatherActivity extends AppCompatActivity implements WeatherView {
    private static final String EXTRA_CITY_ID = "WeatherActivity.EXTRA_CITY_ID";

    @BindView(R.id.tvCity) TextView mTvCity;
    @BindView(R.id.tvDay) TextView mTvDay;
    @BindView(R.id.tvTemperature) TextView mTvTemperature;
    @BindView(R.id.ivWeather) ImageView mIvWeather;
    @BindView(R.id.tvHumidity) TextView mTvHumidity;
    @BindView(R.id.tvClouds) TextView mTvClouds;
    @BindView(R.id.tvWind) TextView mTvWind;

    private String mDegreeFormatter;
    private String mPercentFormatter;
    private String mWindFormatter;

    private WeatherPresenter mPresenter;

    private long mCityId;
    private ProgressDialog mProgressDialog;

    public static void start(Context context, long cityId) {
        Intent starter = new Intent(context, WeatherActivity.class);
        starter.putExtra(EXTRA_CITY_ID, cityId);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        ButterKnife.bind(this);
        initFormatters();
        mPresenter = new WeatherPresenterImpl();
        Intent intent = getIntent();
        if (intent != null) {
            mCityId = intent.getLongExtra(EXTRA_CITY_ID, -1);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.detachView();
    }

    @Override
    public long provideCityId() {
        return mCityId;
    }

    @Override
    public void showWeather(WeatherViewModel weatherViewModel) {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }

        // Location and date
        mTvCity.setText(weatherViewModel.getCity());
        mTvDay.setText(weatherViewModel.getDay());

        // Base weather
        showTemperature(weatherViewModel);
        showWeatherIcon(weatherViewModel);

        // PhenomenaView details
        showHumidity(weatherViewModel);
        showClouds(weatherViewModel);
        showWind(weatherViewModel);
    }

    @Override
    public void showWaiting() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getString(R.string.text_loading));
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();
    }

    private void showWind(WeatherViewModel weatherViewModel) {
        mTvWind.setText(String.format(
                Locale.getDefault(),
                mWindFormatter,
                ViewUtil.getDirectionSymbols(weatherViewModel.getWindDirection()),
                weatherViewModel.getWindSpeed()
        ));
    }

    private void initFormatters() {
        mDegreeFormatter = getString(R.string.formatter_degree);
        mPercentFormatter = getString(R.string.formatter_percent);
        mWindFormatter = getString(R.string.formatter_wind);
    }

    private void showTemperature(WeatherViewModel weatherViewModel) {
        mTvTemperature.setText(String.format(
                Locale.getDefault(), mDegreeFormatter, weatherViewModel.getTemperature()
        ));
    }

    private void showWeatherIcon(WeatherViewModel weatherViewModel) {
        ViewUtil.setWeatherIcon(mIvWeather, weatherViewModel.getPhenomena());
    }

    private void showHumidity(WeatherViewModel weatherViewModel) {
        mTvHumidity.setText(String.format(
                Locale.getDefault(), mPercentFormatter, weatherViewModel.getHumidity()
        ));
    }

    private void showClouds(WeatherViewModel weatherViewModel) {
        mTvClouds.setText(String.format(
                Locale.getDefault(), mPercentFormatter, weatherViewModel.getClouds()
        ));
    }
}
