package io.github.alexeychurchill.weathercards.logic.data;

import java.io.IOException;
import java.util.List;

import io.github.alexeychurchill.weathercards.logic.specific_pojo.OwmCity;

public interface OwmCityRepository extends OwmCitySource {
    OwmCity getCity(long id) throws IOException;
    List<OwmCity> searchCities(String query) throws IOException;
    List<OwmCity> getCitiesByCountry(String country) throws IOException;
    OwmCity saveCity(OwmCity city) throws IOException;
}
