package io.github.alexeychurchill.weathercards.logic.data;

import java.io.IOException;
import java.util.List;

import io.github.alexeychurchill.weathercards.logic.specific_pojo.OwmCity;

public interface OwmCitySource {
    List<OwmCity> getCities() throws IOException;
}
