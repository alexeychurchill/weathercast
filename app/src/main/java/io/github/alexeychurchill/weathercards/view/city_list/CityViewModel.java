package io.github.alexeychurchill.weathercards.view.city_list;

import io.github.alexeychurchill.weathercards.logic.pojo.City;

class CityViewModel {
    private String countryCode;
    private String city;
    private int cityNameSelectionStart = -1;
    private int cityNameSelectionEnd = -1;

    CityViewModel(City city) {
        this.city = city.getCity();
        this.countryCode = city.getCountryCode();
    }

    public String getCity() {
        return city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void citySelection(int start, int end) {
        cityNameSelectionStart = start;
        cityNameSelectionEnd = end;
    }

    public int getCityNameSelectionStart() {
        return cityNameSelectionStart;
    }

    public int getCityNameSelectionEnd() {
        return cityNameSelectionEnd;
    }
}
