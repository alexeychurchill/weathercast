package io.github.alexeychurchill.weathercards.view.city_list;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.style.ReplacementSpan;

public class SearchHighlightSpan extends ReplacementSpan {
    private float mCornerRadius;
    private int mHighlightColor;
    private int mTextColor;

    public SearchHighlightSpan(Context context,
                               @DimenRes int cornerRadiusResId,
                               @ColorRes int highlightColorResId,
                               @ColorRes int textColorResId) {

        this(
                context.getResources().getDimensionPixelSize(cornerRadiusResId),
                ContextCompat.getColor(context, highlightColorResId),
                ContextCompat.getColor(context, textColorResId)
        );

    }

    public SearchHighlightSpan(float cornerRadius,
                               @ColorInt int highlightColor,
                               @ColorInt int textColor) {

        this.mCornerRadius = cornerRadius;
        this.mHighlightColor = highlightColor;
        this.mTextColor = textColor;
    }

    @Override
    public int getSize(@NonNull Paint paint,
                       CharSequence text,
                       @IntRange(from = 0) int start,
                       @IntRange(from = 0) int end,
                       @Nullable Paint.FontMetricsInt fm) {
        copyFontMetrics(paint.getFontMetricsInt(), fm);
        return Math.round(measureText(paint, text, start, end));
    }

    @Override
    public void draw(@NonNull Canvas canvas,
                     CharSequence text,
                     @IntRange(from = 0) int start,
                     @IntRange(from = 0) int end,
                     float x,
                     int top,
                     int y,
                     int bottom,
                     @NonNull Paint paint) {

        RectF highlightRectF = new RectF(x, top, x + measureText(paint, text, start, end),  bottom);
        paint.setColor(mHighlightColor);
        canvas.drawRoundRect(highlightRectF, mCornerRadius, mCornerRadius, paint);
        paint.setColor(mTextColor);
        canvas.drawText(text, start, end, x, y, paint);
    }

    private float measureText(Paint paint, CharSequence text, int start, int end) {
        return paint.measureText(text, start, end);
    }

    private static void copyFontMetrics(Paint.FontMetricsInt from, Paint.FontMetricsInt to) {
        if (from == null || to == null) {
            return;
        }
        to.top = from.top;
        to.ascent = from.ascent;
        to.descent = from.descent;
        to.bottom = from.bottom;
        to.leading = from.leading;
    }
}
