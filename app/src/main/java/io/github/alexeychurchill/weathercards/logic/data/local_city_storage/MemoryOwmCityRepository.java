package io.github.alexeychurchill.weathercards.logic.data.local_city_storage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.github.alexeychurchill.weathercards.logic.data.OwmCityRepository;
import io.github.alexeychurchill.weathercards.logic.data.OwmCitySource;
import io.github.alexeychurchill.weathercards.logic.specific_pojo.OwmCity;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class MemoryOwmCityRepository implements OwmCityRepository {
    private OwmCitySource mCitySource;
    private Map<Long, OwmCity> mData;

    public MemoryOwmCityRepository(OwmCitySource citySource) {
        this.mCitySource = citySource;
    }

    @Override
    public OwmCity getCity(long id) throws IOException {
        return getData().get(id);
    }

    @Override
    public List<OwmCity> getCities() throws IOException {
        return StreamSupport.stream(getData().entrySet())
                .map(Map.Entry::getValue)
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<OwmCity> searchCities(String query) throws IOException {
        if (query == null || query.isEmpty()) {
            return new ArrayList<>();
        }
        return StreamSupport.stream(getData().entrySet())
                .map(Map.Entry::getValue)
                .filter(city -> city.getCity().getCity().contains(query))
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<OwmCity> getCitiesByCountry(String country) throws IOException {
        if (country == null || country.isEmpty()) {
            return new ArrayList<>();
        }
        return StreamSupport.stream(getData().entrySet())
                .map(Map.Entry::getValue)
                .filter(city -> city.getCity().getCountryCode().contentEquals(country))
                .sorted()
                .collect(Collectors.toList());
    };

    @Override
    public OwmCity saveCity(OwmCity city) throws IOException {
        getData().put(city.getOwmId(), city);
        return city;
    }

    private Map<Long, OwmCity> getData() throws IOException {
        if (mData == null) {
            mData = StreamSupport.stream(mCitySource.getCities())
                    .collect(Collectors.toMap(OwmCity::getOwmId, city -> city));
        }
        return mData;
    }
}
