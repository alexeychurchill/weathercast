package io.github.alexeychurchill.weathercards.view.city_list;

import io.github.alexeychurchill.weathercards.view.common.Presenter;

public interface CityListPresenter extends Presenter<CityListView> {
    void choose(int itemIndex);
    void performSearch(String query);
    void cancelSearch();
}
