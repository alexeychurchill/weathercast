package io.github.alexeychurchill.weathercards.view.city_list;

import java.util.List;

import io.github.alexeychurchill.weathercards.logic.data.OwmCityRepository;

public interface CityListView {
    void showLoadProgress();
    void hideLoadProgress();
    void showList(List<CityViewModel> items);
    void showSearchView(String query);
    void hideSearchView();
    OwmCityRepository provideCityRepository();
    void showForecast(long owmId);
}
