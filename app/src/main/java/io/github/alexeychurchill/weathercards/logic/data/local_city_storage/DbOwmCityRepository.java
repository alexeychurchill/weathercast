package io.github.alexeychurchill.weathercards.logic.data.local_city_storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.github.alexeychurchill.weathercards.logic.data.OwmCityRepository;
import io.github.alexeychurchill.weathercards.logic.db.DbContract;
import io.github.alexeychurchill.weathercards.logic.db.DbConverters;
import io.github.alexeychurchill.weathercards.logic.db.DbHelper;
import io.github.alexeychurchill.weathercards.logic.specific_pojo.OwmCity;

public class DbOwmCityRepository implements OwmCityRepository {
    private static final String SQL_SELECTION_BY_ID = DbContract.CityColumns._ID  + " = ?";
    private static final String SQL_SELECTION_BY_COUNTRY = DbContract.CityColumns.COUNTRY + " = ?";
    private static final String SQL_SEARCH_CITIES = DbContract.CityColumns.CITY + " LIKE %?%";
    private DbHelper mDbHelper;

    public DbOwmCityRepository(Context context) {
        mDbHelper = new DbHelper(context);
    }

    @Override
    public List<OwmCity> getCities() throws IOException {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        Cursor cursor = database.query(
                DbContract.TABLE_NAME_CITY,
                null,
                null,
                null,
                null,
                null,
                null
        );

        List<OwmCity> owmCities = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            do {
                owmCities.add(DbConverters.cursorToOwmCity(cursor));
            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }

        database.close();

        return owmCities;
    }

    @Override
    public OwmCity getCity(long id) throws IOException {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        String[] args = new String[]{String.valueOf(id)};
        Cursor cursor = database.query(
                DbContract.TABLE_NAME_CITY,
                null,
                SQL_SELECTION_BY_ID,
                args,
                null,
                null,
                null
        );

        OwmCity owmCity = null;
        if (cursor != null && cursor.moveToFirst()) {
            owmCity = DbConverters.cursorToOwmCity(cursor);
        }

        if (cursor != null) {
            cursor.close();
        }

        database.close();

        return owmCity;
    }

    @Override
    public List<OwmCity> searchCities(String query) throws IOException {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        String[] args = new String[]{query};
        Cursor cursor = database.query(
                DbContract.TABLE_NAME_CITY,
                null,
                SQL_SEARCH_CITIES,
                args,
                null,
                null,
                null
        );

        List<OwmCity> owmCities = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            do {
                OwmCity owmCity = DbConverters.cursorToOwmCity(cursor);
                owmCities.add(owmCity);
            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }

        return owmCities;
    }

    @Override
    public List<OwmCity> getCitiesByCountry(String country) throws IOException {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        String[] args = new String[]{country};
        Cursor cursor = database.query(
                DbContract.TABLE_NAME_CITY,
                null,
                SQL_SELECTION_BY_COUNTRY,
                args,
                null,
                null,
                null
        );

        List<OwmCity> owmCities = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            do {
                OwmCity owmCity = DbConverters.cursorToOwmCity(cursor);
                owmCities.add(owmCity);
            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }

        return owmCities;
    }

    @Override
    public OwmCity saveCity(OwmCity city) throws IOException {
        ContentValues values = DbConverters.owmCityToContentValues(city);
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        long id = database.insert(DbContract.TABLE_NAME_CITY, null, values);
        database.close();
        if (id == -1) {
            throw new IOException("Database write exception!");
        }
        return getCity(id);
    }
}
