package io.github.alexeychurchill.weathercards.view.weather;

import java.text.SimpleDateFormat;
import java.util.Locale;

import io.github.alexeychurchill.weathercards.Config;
import io.github.alexeychurchill.weathercards.logic.GetCurrentWeatherUseCase;
import io.github.alexeychurchill.weathercards.logic.common.UseCase;
import io.github.alexeychurchill.weathercards.logic.data.forecast.RetrofitForecastSource;
import io.github.alexeychurchill.weathercards.logic.pojo.City;
import io.github.alexeychurchill.weathercards.logic.pojo.Weather;

public class WeatherPresenterImpl implements WeatherPresenter {
    private static final String FORMAT_DAY = "HH:mm, EEE, dd MMM yyyy";
    private static final double KELVIN_ZERO = -273.15;
    private WeatherView mView;

    private GetCurrentWeatherUseCase mCurrentWeatherUseCase =
            new GetCurrentWeatherUseCase(new RetrofitForecastSource(Config.API_KEY));

    private UseCase.Callback<GetCurrentWeatherUseCase.CurrentWeather> mWeatherCallback =
            new UseCase.Callback<GetCurrentWeatherUseCase.CurrentWeather>() {
        @Override
        public void onSuccess(GetCurrentWeatherUseCase.CurrentWeather weather) {
            showWeather(weather);
        }

        @Override
        public void onError(Throwable throwable) {
            // TODO: Handle error
        }
    };

    @Override
    public void attachView(WeatherView view) {
        mView = view;
        if (mView == null) {
            return;
        }
        mView.showWaiting();
        long cityId = mView.provideCityId();
        mCurrentWeatherUseCase.execute(cityId, mWeatherCallback);
    }

    @Override
    public void detachView() {
        mView = null;
        mCurrentWeatherUseCase.terminate();
    }

    private void showWeather(GetCurrentWeatherUseCase.CurrentWeather weather) {
        if (mView == null) {
            return;
        }
        WeatherViewModel weatherViewModel = buildWeatherViewModel(weather);
        mView.showWeather(weatherViewModel);
    }

    private WeatherViewModel buildWeatherViewModel(
            GetCurrentWeatherUseCase.CurrentWeather currentWeather) {

        WeatherViewModel model = new WeatherViewModel();

        City city = currentWeather.getCity();
        Weather weather = currentWeather.getWeather();

        model.setCity(city.getCity());
        model.setClouds(weather.getClouds());

        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_DAY, Locale.getDefault());
        model.setDay(dateFormat.format(weather.getTime().getTime()));

        model.setTemperature((int) (weather.getTemperature() + KELVIN_ZERO));
        model.setHumidity(weather.getHumidity());
        model.setPhenomena(weather.getPhenomena());

        WindDirection windDirection = WindDirection.fromDegrees(weather.getWindDirection());
        model.setWindDirection(windDirection);

        model.setWindSpeed((int) weather.getWindSpeed());

        return model;
    }
}
