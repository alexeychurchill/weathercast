package io.github.alexeychurchill.weathercards.logic.pojo;

import com.google.gson.annotations.SerializedName;

public class City {
    @SerializedName("id")
    private long id;
    @SerializedName("coord")
    private Coordinates coordinates;
    @SerializedName("country")
    private String countryCode;
    @SerializedName("name")
    private String city;

    public City() {
    }

    /**
     * DEPRECATED! Consider to use {@link #City(long, Coordinates, String, String)}!
     * TODO: REFACTOR!
     */
    @Deprecated
    public City(long id, double latitude, double longitude, String countryCode, String city) {
        this(id, new Coordinates(latitude, longitude), countryCode, city);
    }

    public City(long id, Coordinates coordinates, String countryCode, String city) {
        this.id = id;
        this.coordinates = coordinates;
        this.countryCode = countryCode;
        this.city = city;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Deprecated
    public void setLatitude(double latitude) {
        coordinates.setLatitude(latitude);
    }

    @Deprecated
    public void setLongitude(double longitude) {
        coordinates.setLongitude(longitude);
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getId() {
        return id;
    }

    @Deprecated
    public double getLatitude() {
        return coordinates.getLatitude();
    }

    @Deprecated
    public double getLongitude() {
        return coordinates.getLongitude();
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", coordinates=" + coordinates +
                ", countryCode='" + countryCode + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
