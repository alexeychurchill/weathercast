package io.github.alexeychurchill.weathercards.logic;

import android.util.Log;
import android.util.Pair;

import java.util.Calendar;

import io.github.alexeychurchill.weathercards.logic.common.UseCase;
import io.github.alexeychurchill.weathercards.logic.data.forecast.ForecastSource;
import io.github.alexeychurchill.weathercards.logic.pojo.City;
import io.github.alexeychurchill.weathercards.logic.pojo.CityForecast;
import io.github.alexeychurchill.weathercards.logic.pojo.Weather;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java8.util.stream.StreamSupport;

public class GetCurrentWeatherUseCase implements
        UseCase<Long, GetCurrentWeatherUseCase.CurrentWeather> {

    public static class CurrentWeather {
        private City city;
        private Weather weather;

        public CurrentWeather(City city, Weather weather) {
            this.city = city;
            this.weather = weather;
        }

        public City getCity() {
            return city;
        }

        public Weather getWeather() {
            return weather;
        }
    }

    private ForecastSource mForecastSource;
    private Disposable mDisposable;

    public GetCurrentWeatherUseCase(ForecastSource forecastSource) {
        this.mForecastSource = forecastSource;
    }

    @Override
    public void execute(Long argument, Callback<CurrentWeather> callback) {
        terminate();
        mDisposable = mForecastSource.getCityForecast(argument)
                .map(this::getCurrentWeather)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(callback::onSuccess);
    }

    @Override
    public void terminate() {
        if (mDisposable != null) {
            mDisposable.dispose();
            mDisposable = null;
        }
    }

    private CurrentWeather getCurrentWeather(CityForecast cityForecast) {
        Calendar calendar = Calendar.getInstance();
        int today = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        return StreamSupport.stream(cityForecast.getWeatherList())
                .filter(weather -> weather.getTime().get(Calendar.DAY_OF_MONTH) == today)
                .map(weather -> Pair.create(
                        Math.abs(weather.getTime().get(Calendar.HOUR_OF_DAY) - hour),
                        weather))
                .min((o1, o2) -> o1.first - o2.first)
                .map(pair -> pair.second)
                .map(weather -> new CurrentWeather(cityForecast.getCity(), weather))
                .get();
    }
}
