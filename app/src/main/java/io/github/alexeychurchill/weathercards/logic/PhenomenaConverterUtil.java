package io.github.alexeychurchill.weathercards.logic;

import io.github.alexeychurchill.weathercards.logic.pojo.Phenomena;

public final class PhenomenaConverterUtil {
    public static Phenomena toPhenomena(int id) {
        switch (id / 100) {
            case 2:
                return Phenomena.THUNDER;

            case 3:
                return Phenomena.DRIZZLE;

            case 5:
                return id % 100 / 10 < 2 ?
                        Phenomena.RAIN :
                        Phenomena.SHOWER_RAIN;

            case 6:
                return Phenomena.SNOW;

            case 7:
                return Phenomena.MIST;

            case 8:
                return id % 100 == 0 ?
                        Phenomena.SUNNY :
                        id % 100 < 3 ?
                                Phenomena.CLOUDY :
                                Phenomena.CLOUDS;

            default:
                return null;
        }
    }
}
