package io.github.alexeychurchill.weathercards.logic.pojo;

import java.util.Calendar;

public class Weather {
    private Calendar time;

    private double temperature;
    private double minimalTemperature;
    private double maximalTemperature;
    private double pressure;
    private int humidity;

    private Phenomena phenomena;

    private int clouds;

    private double windSpeed;
    private double windDirection;

    public Calendar getTime() {
        return time;
    }

    public double getTemperature() {
        return temperature;
    }

    public double getMinimalTemperature() {
        return minimalTemperature;
    }

    public double getMaximalTemperature() {
        return maximalTemperature;
    }

    public double getPressure() {
        return pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public Phenomena getPhenomena() {
        return phenomena;
    }

    public int getClouds() {
        return clouds;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public double getWindDirection() {
        return windDirection;
    }

    private Weather() {
    }

    public static class Builder {
        private Weather weather = new Weather();

        public Builder setTime(long timeMs) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timeMs);
            weather.time = calendar;
            return this;
        }

        public Builder setTemperature(double temperature) {
            weather.temperature = temperature;
            return this;
        }

        public Builder setMinimalTemperature(double temperature) {
            weather.minimalTemperature = temperature;
            return this;
        }

        public Builder setMaximalTemperature(double temperature) {
            weather.maximalTemperature = temperature;
            return this;
        }

        public Builder setPressure(double pressure) {
            weather.pressure = pressure;
            return this;
        }

        public Builder setHumidity(int humidity) {
            weather.humidity = humidity;
            return this;
        }

        public Builder setPhenomena(Phenomena phenomena) {
            weather.phenomena = phenomena;
            return this;
        }

        public Builder setClouds(int clouds) {
            weather.clouds = clouds;
            return this;
        }

        public Builder setWindSpeed(double windSpeed) {
            weather.windSpeed = windSpeed;
            return this;
        }

        public Builder setWindDirection(double windDirection) {
            weather.windDirection = windDirection;
            return this;
        }

        public Weather build() {
            Weather weather = this.weather;
            this.weather = new Weather();
            return weather;
        }
    }
}
