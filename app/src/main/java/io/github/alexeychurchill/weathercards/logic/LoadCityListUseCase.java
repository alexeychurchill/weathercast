package io.github.alexeychurchill.weathercards.logic;

import java.util.List;

import io.github.alexeychurchill.weathercards.logic.common.UseCase;
import io.github.alexeychurchill.weathercards.logic.data.OwmCitySource;
import io.github.alexeychurchill.weathercards.logic.rx.RxCitiesUtils;
import io.github.alexeychurchill.weathercards.logic.specific_pojo.OwmCity;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoadCityListUseCase implements UseCase<Void, List<OwmCity>> {
    private OwmCitySource mOwmCitySource;
    private Disposable mDisposable;

    public LoadCityListUseCase(OwmCitySource dataSource) {
        this.mOwmCitySource = dataSource;
    }

    @Override
    public void execute(Void argument, Callback<List<OwmCity>> callback) {
        mDisposable = createSingle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(callback::onError)
                .subscribe(callback::onSuccess);
    }

    @Override
    public void terminate() {
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }

    private Single<List<OwmCity>> createSingle() {
        return RxCitiesUtils.createLoadCitiesSingle(mOwmCitySource);
    }
}
