package io.github.alexeychurchill.weathercards.logic.pojo;

public enum Phenomena {
    CLOUDS,
    CLOUDY,
    DRIZZLE,
    MIST,
    RAIN,
    SHOWER_RAIN,
    SNOW,
    SUNNY,
    THUNDER
}
