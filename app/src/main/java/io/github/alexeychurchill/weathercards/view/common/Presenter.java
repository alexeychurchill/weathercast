package io.github.alexeychurchill.weathercards.view.common;

public interface Presenter <T> {
    void attachView(T view);
    void detachView();
}
