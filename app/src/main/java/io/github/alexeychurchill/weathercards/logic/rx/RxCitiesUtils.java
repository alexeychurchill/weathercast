package io.github.alexeychurchill.weathercards.logic.rx;

import java.util.List;

import io.github.alexeychurchill.weathercards.logic.data.OwmCityRepository;
import io.github.alexeychurchill.weathercards.logic.data.OwmCitySource;
import io.github.alexeychurchill.weathercards.logic.specific_pojo.OwmCity;
import io.reactivex.Single;

public final class RxCitiesUtils {

    public static Single<List<OwmCity>> createLoadCitiesSingle(OwmCitySource dataSource) {
        return Single.create(
                emitter -> {
                    try {
                        List<OwmCity> owmCities = dataSource.getCities();
                        if (emitter.isDisposed()) {
                            return;
                        }
                        emitter.onSuccess(owmCities);
                    } catch (Throwable throwable) {
                        emitter.onError(throwable);
                    }
                }
        );
    }

    public static Single<OwmCity> createSaveCitySingle(OwmCityRepository repository, OwmCity city) {
        return Single.create(emitter -> {
            try {
                if (emitter.isDisposed()) {
                    return;
                }
                emitter.onSuccess(repository.saveCity(city));
            } catch (Throwable throwable) {
                emitter.onError(throwable);
            }
        });
    }
}
