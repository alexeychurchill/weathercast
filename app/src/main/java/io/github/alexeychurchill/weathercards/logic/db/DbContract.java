package io.github.alexeychurchill.weathercards.logic.db;

import android.provider.BaseColumns;

public interface DbContract {
    String TYPE_INT = "INTEGER";
    String TYPE_STRING = "TEXT";
    String TYPE_FLOAT = "REAL";

    String DATABASE_NAME = "WeathercastDB";
    int DATABASE_VERSION = 1;

    // City table

    String TABLE_NAME_CITY = "city";

    interface CityColumns extends BaseColumns {
        String CITY = "city";
        String COUNTRY = "country";
        String LATITUDE = "latitude";
        String LONGITUDE = "longitude";
        String OWM_ID = "owm_id";
    }

    String SQL_CREATE_TABLE_CITY = "CREATE TABLE " + TABLE_NAME_CITY + "(" +
            CityColumns._ID + " " + TYPE_INT + " PRIMARY KEY AUTOINCREMENT, " +
            CityColumns.CITY + " " + TYPE_STRING + ", " +
            CityColumns.COUNTRY + " " + TYPE_STRING + ", " +
            CityColumns.LATITUDE + " " + TYPE_FLOAT + ", " +
            CityColumns.LONGITUDE + " " + TYPE_FLOAT + ", " +
            CityColumns.OWM_ID + " " + TYPE_FLOAT + " UNIQUE" +
            ")";
}
