package io.github.alexeychurchill.weathercards.view.weather;

import android.widget.ImageView;

import io.github.alexeychurchill.weathercards.R;
import io.github.alexeychurchill.weathercards.logic.pojo.Phenomena;

public final class ViewUtil {
    public static void setWeatherIcon(ImageView view, Phenomena phenomenaView) {
        switch (phenomenaView) {
            case CLOUDS:
                view.setImageResource(R.drawable.weather_clouds);
                return;
            case CLOUDY:
                view.setImageResource(R.drawable.weather_cloudy);
                return;
            case DRIZZLE:
                view.setImageResource(R.drawable.weather_drizzle);
                return;
            case MIST:
                view.setImageResource(R.drawable.weather_mist);
                return;
            case RAIN:
                view.setImageResource(R.drawable.weather_rain);
                return;
            case SHOWER_RAIN:
                view.setImageResource(R.drawable.weather_shower_rain);
                return;
            case SNOW:
                view.setImageResource(R.drawable.weather_snow);
                return;
            case SUNNY:
                view.setImageResource(R.drawable.weather_sunny);
                return;
            case THUNDER:
                view.setImageResource(R.drawable.weather_thunder);
                return;
        }
    }

    public static String getDirectionSymbols(WindDirection direction) {
        switch (direction) {
            case EAST:
                return "E";
            case NORTH:
                return "N";
            case NORTH_EAST:
                return "NE";
            case NORTH_WEST:
                return "NW";
            case SOUTH:
                return "S";
            case SOUTH_EAST:
                return "SE";
            case SOUTH_WEST:
                return "SW";
            case WEST:
                return "W";
            default:
                return "N/A";
        }
    }
}
