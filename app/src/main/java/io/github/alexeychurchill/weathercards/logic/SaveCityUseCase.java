package io.github.alexeychurchill.weathercards.logic;

import io.github.alexeychurchill.weathercards.logic.common.UseCase;
import io.github.alexeychurchill.weathercards.logic.data.OwmCityRepository;
import io.github.alexeychurchill.weathercards.logic.rx.RxCitiesUtils;
import io.github.alexeychurchill.weathercards.logic.specific_pojo.OwmCity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SaveCityUseCase implements UseCase<OwmCity, OwmCity> {
    private OwmCityRepository mRepository;
    private Disposable mSaveDisposable;

    public SaveCityUseCase(OwmCityRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public void execute(OwmCity argument, Callback<OwmCity> callback) {
        terminate();
        mSaveDisposable = RxCitiesUtils.createSaveCitySingle(mRepository, argument)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(callback::onError)
                .subscribe(callback::onSuccess);
    }

    @Override
    public void terminate() {
        if (mSaveDisposable != null) {
            mSaveDisposable.dispose();
            mSaveDisposable = null;
        }
    }
}
