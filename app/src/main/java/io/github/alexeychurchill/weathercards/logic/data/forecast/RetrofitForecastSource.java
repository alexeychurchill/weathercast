package io.github.alexeychurchill.weathercards.logic.data.forecast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.github.alexeychurchill.weathercards.logic.OWMApi;
import io.github.alexeychurchill.weathercards.logic.json.WeatherDeserializer;
import io.github.alexeychurchill.weathercards.logic.pojo.CityForecast;
import io.github.alexeychurchill.weathercards.logic.pojo.Weather;
import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitForecastSource implements ForecastSource {
    private String mApiKey;
    private OWMApi mOwmApi;

    public RetrofitForecastSource(String apiKey) {
        mApiKey = apiKey;
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Weather.class, new WeatherDeserializer())
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(OWMApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        mOwmApi = retrofit.create(OWMApi.class);
    }

    @Override
    public Observable<CityForecast> getCityForecast(long id) {
        return mOwmApi.getForecast(mApiKey, id);
    }
}
