package io.github.alexeychurchill.weathercards.view.weather;

import io.github.alexeychurchill.weathercards.view.common.Presenter;

public interface WeatherPresenter extends Presenter<WeatherView> {
}
